FROM openjdk:8-jdk-alpine
VOLUME /tmp
ENV LANG en_GB.UTF-8
RUN apk add --update && rm -rf /var/cache/apk/*
RUN echo "Africa/Harare" > /etc/timezone
ADD stewardhealthsystemweb-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.config.location= /data/stewardhealth/web/conf/application.properties","-jar","/app.jar"]
