
INSERT INTO role (created_by, created_date, last_modified_by, last_modified_date, description, is_role_limited, name)
    SELECT * FROM (SELECT 'system_default', '2020-01-01', 'system_default1', '2020-01-02', 'default1', false, 'DEFAULT') as ug
    WHERE NOT EXISTS (
            SELECT created_date FROM role WHERE created_date = '2020-01-01' AND created_by = 'system_default'
    ) LIMIT 1;

INSERT INTO users (created_by, created_date, last_modified_by, last_modified_date, dob, is_ecocash_customer, first_log_in,
                   first_name, gender, last_name, password, status, username, role_id)
    SELECT * FROM (SELECT 'system_default', '2020-01-01', 'system_default1', '2020-01-02', '2020-01-03', true, false, 'System', 'MALE', 'User', 'home', 'ACTIVE', 'admin', 1) as u
    WHERE NOT EXISTS (
            SELECT created_date FROM users WHERE created_date = '2020-01-01' AND created_by = 'system_default'
    ) LIMIT 1;