package com.xelttsh.vms.config;

import com.xelttsh.vms.commons.converter.LocalDateToStringConverter;
import com.xelttsh.vms.commons.converter.StringToLocalDateConverter;
import com.xelttsh.vms.commons.converter.StringToLocalDateTimeConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToLocalDateConverter());
        registry.addConverter(new LocalDateToStringConverter());
        registry.addConverter(new StringToLocalDateTimeConverter());
    }
}
