package com.xelttsh.vms.config;

import com.xelttsh.vms.commons.enums.security.AccessRight;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.model.Role;
import com.xelttsh.vms.model.User;
import com.xelttsh.vms.service.TokenService;
import com.xelttsh.vms.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        try {
            String username = authentication.getName();
            String password = authentication.getCredentials().toString();

//                String token = tokenService.getToken(username, password);
//                httpSession.setAttribute("TOKEN", token);
            Optional<User> userOptional = userService.findByUsername(username);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                httpSession.setAttribute(Constants.SESSION_USERNAME, user.getUsername());
                httpSession.setAttribute(Constants.SESSION_EMAIL, user.getEmail());

                Collection<? extends GrantedAuthority> authorities = getAuthorities(Collections.singletonList(user.getRole()));
                if (password.equals(user.getPassword())) {
                    if(Constants.DEFAULT_ADMIN.equals(username)) {
                        List<String> values = Arrays.stream(AccessRight.values()).map(Enum::name).collect(Collectors.toList());
                        authorities = getGrantedAuthorities(values);
                    }
                    return new UsernamePasswordAuthenticationToken(user.getFirstName(), password, authorities);
                }
            }
            return null;
        } catch (ResourceAccessException e) {
            log.error("", e);
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }

    private List<String> getPrivileges(Collection<Role> roles) {
        List<String> privileges = new ArrayList<>();
        List<AccessRight> collection = new ArrayList<>();
        for (Role role : roles) {
            collection.addAll(role.getAccessRights());
        }
        for (AccessRight item : collection) {
            privileges.add(item.name());
        }
        return privileges;
    }


    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

}
