package com.xelttsh.vms.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionTranslationFilter extends ExceptionMappingAuthenticationFailureHandler {


    @Override
    public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse res, AuthenticationException ex) throws IOException, ServletException {

        logger.info("[AuthenticationFailure]: " + "[Username]: " + req.getParameter("username") + " [Error message]:  " + ex);
        res.sendRedirect(req.getContextPath() + "/login?error=");


    }

}
