package com.xelttsh.vms.service;

import com.xelttsh.vms.commons.data.DocumentDTO;
import com.xelttsh.vms.model.Document;

import java.util.List;

public interface DocumentService {
    DocumentDTO create(DocumentDTO documentDto);
    List<Document> findAll();
    List<Document> findByEntityId(String entityId);
}
