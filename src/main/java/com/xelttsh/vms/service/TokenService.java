package com.xelttsh.vms.service;

public interface TokenService {

    String getToken(String username, String password);

}
