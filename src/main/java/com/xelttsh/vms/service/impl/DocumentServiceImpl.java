package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.commons.data.DocumentDTO;
import com.xelttsh.vms.commons.data.ApiResponse;
import com.xelttsh.vms.model.Document;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {
    private final CoreInvoker coreInvoker;
    @Value("${server.endpoint.url}")
    private String serverEndPoint;

    @Override
    public DocumentDTO create(DocumentDTO documentDto) {
        String url = serverEndPoint + "/api/v1/document/create";
        return coreInvoker.invoke(documentDto, url, HttpMethod.POST, new ParameterizedTypeReference<ApiResponse<DocumentDTO>>() {});

    }

    @Override
    public List<Document> findAll() {
        String url = serverEndPoint + "/api/v1/document/list";
        return  coreInvoker.invoke(null, url, HttpMethod.GET,  new ParameterizedTypeReference<ApiResponse<List<Document>>>(){});
    }

    @Override
    public List<Document> findByEntityId(String entityId) {
        String url = serverEndPoint + "/api/v1/document/findByEntityId/" + entityId;
        return  coreInvoker.invoke(null, url, HttpMethod.GET,  new ParameterizedTypeReference<ApiResponse<List<Document>>>(){});
    }


}
