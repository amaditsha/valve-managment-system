package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.model.User;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.service.UserService;
import com.xelttsh.vms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final CoreInvoker coreInvoker;
    @Value("${server.endpoint.url}")
    private String serverEndPoint;

    private final UserRepository userRepository;


    @Override
    public Optional<User> findById(Object id) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll(int page, int size) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> create(User user) {
        user = userRepository.save(user);
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> update(User user) {
        user = userRepository.save(user);
        return Optional.ofNullable(user);
    }

    @Override
    public void delete(User user) {

    }

    @Override
    public Optional<User> findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return Optional.ofNullable(user);
    }
}
