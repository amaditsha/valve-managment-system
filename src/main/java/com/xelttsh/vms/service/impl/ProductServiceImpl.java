package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.commons.data.ProductSearch;
import com.xelttsh.vms.commons.utils.Util;
import com.xelttsh.vms.service.CategoryService;
import com.xelttsh.vms.commons.enums.EntityStatus;
import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Category;
import com.xelttsh.vms.model.Product;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.repository.ProductRepository;
import com.xelttsh.vms.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {


    @Value("${server.endpoint.url}")
    private String serverEndPoint;
    private final CoreInvoker coreInvoker;

    private final ProductRepository productRepository;

    private final CategoryService categoryService;


    @Override
    public Product create(Product product) {
        log.info("::: product:: create :: product : {}", product);
        return  productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        return  productRepository.save(product);
    }

    @Override
    public void delete(String productId) {
        productRepository.findById(productId)
                .map(product -> {
                    product.setStatus(EntityStatus.DELETED);
                    return productRepository.save(product);
                });
    }

    @Override
    public Product findById(String productId) {
        return  productRepository.findById(productId).orElse(null);
    }

    @Override
    public List<Product> getPackages() {
        return  productRepository.findAll().stream().filter(product -> !product.getStatus().equals(EntityStatus.DELETED)).collect(Collectors.toList());
    }

    @Override
    public List<Product> findAllByCategory_ValveType(PlantValveType type) {
        return productRepository.findProductByCategory_PlantValveType(type);
    }

    @Override
    public List<Product> findAllByCategory_CategoryId(Long productCategoryId) {
        return  productRepository.findProductByCategory_CategoryId(productCategoryId);
    }

    @Override
    public List<Product> findByAlphaNumericAndSection(String alphaNumeric, PlantValveType section) {
        return  productRepository.findByAlphaNumericAndSection(alphaNumeric, section, EntityStatus.DELETED);
    }

    @Override
    public List<Product> search(ProductSearch search) {
        log.info("Query: AlphaNumeric {}, valve type {}, kksNo {}, service {}, From {}, To {}, type {}", Util.isEmpty(search.getAlphaNumeric()), Util.isEmpty(search.getValveType()), Util.isEmpty(search.getKksNo()), Util.isEmpty(search.getService()), search.getDateFrom(), search.getDateTo(), search.getType());
        return productRepository.search(Util.isEmpty(search.getAlphaNumeric()), Util.isEmpty(search.getValveType()), Util.isEmpty(search.getKksNo()), Util.isEmpty(search.getService()), search.getDateFrom(), search.getDateTo(), search.getType());
    }

    @Override
    public List<Product> findProductsByCategory_PlantValveTypeAndDate(PlantValveType type, LocalDateTime startTime, LocalDateTime endTime) {
        return productRepository.findProductsByCategory_PlantValveTypeAndDate(type, startTime, endTime);
    }


}
