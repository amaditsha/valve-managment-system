package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.commons.data.ApiResponse;
import com.xelttsh.vms.model.Parameter;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.repository.ParameterRepository;
import com.xelttsh.vms.service.ParameterService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParameterServiceImpl implements ParameterService {

    private final ParameterRepository parameterRepository;

    @Override
    public List<Parameter> findAll() {
        return parameterRepository.findAll();
    }

    @Override
    public Parameter create(Parameter parameter) {
        return parameterRepository.save(parameter);
    }

    @Override
    public Parameter findById(Long id) {
        return  parameterRepository.findById(id).orElse(null);
    }

    @Override
    public Parameter update(Parameter parameter) {
        return parameterRepository.save(parameter);
    }

    @Override
    public void delete(Long parameterId) {
       parameterRepository.deleteById(parameterId);
    }
}
