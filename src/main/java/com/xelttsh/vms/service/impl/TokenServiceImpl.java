package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.commons.data.ApiResponse;
import com.xelttsh.vms.commons.exceptions.AccessDeniedException;
import com.xelttsh.vms.commons.exceptions.BusinessException;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.service.TokenService;
import com.xelttsh.vms.commons.data.TokenRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TokenServiceImpl implements TokenService {

    @Autowired
    private CoreInvoker coreInvoker;
    @Value("${self.service.core.generatetoken.url}")
    private String getTokenUrl;

    @Override
    public String getToken(String username,String password){
        try {
            TokenRequest tokenRequest = new TokenRequest(username, password);
            return coreInvoker.invoke(tokenRequest, getTokenUrl, HttpMethod.POST, new ParameterizedTypeReference<ApiResponse<String>>() {
            });
        }catch(BusinessException be){
            throw new AccessDeniedException(be.getMessage());
        }
    }

}
