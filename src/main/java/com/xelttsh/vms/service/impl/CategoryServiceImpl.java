package com.xelttsh.vms.service.impl;


import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Category;
import com.xelttsh.vms.repository.CategoryRepository;
import com.xelttsh.vms.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@RequestMapping()
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;


    @Override
    public Category create(Category category) {
        return  categoryRepository.save(category);
    }

    @Override
    public Category update(Category category) {
        return categoryRepository.findById(category.getCategoryId())
                .map(productCategory1 -> {
                    category.setDateCreated(productCategory1.getDateCreated());
                    return  categoryRepository.save(category);
                }).orElseGet(() -> categoryRepository.save(category));

    }

    @Override
    public Category delete(Long productCategoryId) {
        return categoryRepository.findById(productCategoryId)
                .map(productCategory -> {
                    categoryRepository.deleteById(productCategoryId);
                    return productCategory;
                }).orElse(null);

    }


    @Override
    public Category findById(Long productCategoryId) {
        return  categoryRepository.findById(productCategoryId).orElse(null);
    }

    @Override
    public List<Category> findAll() {
        return  categoryRepository.findAll();
    }

    @Override
    public List<Category> findAllByPlantValveType(PlantValveType type) {
        return categoryRepository.findAllByPlantValveType(type);
    }

}
