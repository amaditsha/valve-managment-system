package com.xelttsh.vms.service.impl;

import com.xelttsh.vms.model.Role;
import com.xelttsh.vms.invorker.CoreInvoker;
import com.xelttsh.vms.repository.RoleRepository;
import com.xelttsh.vms.service.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleServiceImpl implements RoleService {
    private final CoreInvoker coreInvoker;
    @Value("${server.endpoint.url}")
    private String serverEndPoint;

    private final RoleRepository roleRepository;

    @Override
    public Optional<Role> findById(Object id) {
        return roleRepository.findById((Long)id);
    }

    @Override
    public List<Role> findAll(int page, int size) {
        return null;
    }
    @Override
    public List<Role> findAll() {
//        final String url = String.format("%s/api/v1/role/list", serverEndPoint);
        return  roleRepository.findAll();
    }

    @Override
    public Optional<Role> findByName(String name) {
        return roleRepository.findByName(name);
    }


    @Override
    public Optional<Role> create(Role role) {
        role = roleRepository.save(role);
        return Optional.ofNullable(role);
    }

    @Override
    public Optional<Role> update(Role role) {
        return Optional.ofNullable(roleRepository.save(role));
    }

    @Override
    public void delete(Role role) {
        roleRepository.delete(role);
    }
}
