package com.xelttsh.vms.service;

import com.xelttsh.vms.commons.service.AppService;
import com.xelttsh.vms.model.User;

import java.util.Optional;

public interface UserService extends AppService<User> {
    Optional<User> findByUsername(String username);
}
