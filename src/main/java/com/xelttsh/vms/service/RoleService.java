package com.xelttsh.vms.service;

import com.xelttsh.vms.commons.service.AppService;
import com.xelttsh.vms.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService extends AppService<Role> {
    List<Role> findAll();
    Optional<Role> findByName(String name);
}
