package com.xelttsh.vms.service;


import com.xelttsh.vms.commons.data.ProductSearch;
import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Product;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductService {
    Product create(Product product);
    Product update(Product product);
    void delete(String productId);
    Product findById(String productId);
    List <Product> getPackages();
    List<Product> findAllByCategory_ValveType(PlantValveType type);
    List<Product> findAllByCategory_CategoryId(Long productCategoryId);
    List<Product> findByAlphaNumericAndSection(String alphaNumeric, PlantValveType section);
    List<Product> search(ProductSearch search);
    List<Product> findProductsByCategory_PlantValveTypeAndDate(PlantValveType type, LocalDateTime startTime, LocalDateTime endTime);

}
