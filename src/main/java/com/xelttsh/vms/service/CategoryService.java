package com.xelttsh.vms.service;


import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Category;

import java.util.List;


public interface CategoryService {

    Category create(Category category);
    Category update(Category category);
    Category delete(Long productCategoryId);
    Category findById(Long productCategoryId);
    List <Category> findAll();
    List <Category> findAllByPlantValveType(PlantValveType type);

}
