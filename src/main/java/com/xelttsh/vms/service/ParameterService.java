package com.xelttsh.vms.service;

import com.xelttsh.vms.model.Parameter;

import java.util.List;

public interface ParameterService {
    List<Parameter> findAll();
    Parameter create(Parameter parameter);
    Parameter findById(Long id);
    Parameter update(Parameter parameter);
    void delete(Long parameterId);

}
