package com.xelttsh.vms.controllers;

import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.data.ChangePassword;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.commons.utils.Util;
import com.xelttsh.vms.mail.EmailService;
import com.xelttsh.vms.model.User;
import com.xelttsh.vms.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@Slf4j
@RequiredArgsConstructor
public class MainController {

    private final UserService userService;
    private final EmailService emailService;

    @GetMapping("/")
    public String index(HttpSession httpSession) {
        log.info("In index. Loading the index page");
        String username = (String)httpSession.getAttribute(Constants.SESSION_USERNAME);

        Optional<User> optionalUser = userService.findByUsername(username);

        return optionalUser.map(user -> {
            if(user.getIsPasswordChanged() == null || !user.getIsPasswordChanged()) {
                return "redirect:/changePassword";
            }
            return "main/index";
        }).orElseGet(() -> {
            return "main/index";
        });
    }

    @GetMapping("/changePassword")
    public String renderChangePassword(Model model) {
        log.info("In render change password. ");
        model.addAttribute("changePassword", new ChangePassword());
        return "main/changepassword";
    }

    @PostMapping("/changePassword")
    public String changePassword(@Valid ChangePassword changePassword, HttpSession httpSession, RedirectAttributes redirectAttributes) {
        log.info("In change password. Request: {}", changePassword);
        if(!changePassword.getConfirmPassword().equals(changePassword.getPassword())) {
            redirectAttributes.addFlashAttribute("message", new Message(String.format("Password mismatch."), MessageType.ERROR));
            return "redirect:/changePassword";
        }
        String username = (String)httpSession.getAttribute(Constants.SESSION_USERNAME);
        return userService.findByUsername(username)
                .map(user -> {
                    if(!changePassword.getOldPassword().equals(user.getPassword())) {
                        redirectAttributes.addFlashAttribute("message", new Message(String.format("Old password is incorrect."), MessageType.ERROR));
                        return "redirect:/changePassword";
                    }
                    user.setPassword(changePassword.getPassword());
                    user.setIsPasswordChanged(true);
                    userService.update(user);

                    return "redirect:/";
                }).orElseGet(() -> {
                    return "redirect:/";
        });
    }

    @GetMapping("/password/reset")
    public String renderResetPassword(Model model) {
        log.info("In render reset password. ");
        model.addAttribute("changePassword", new ChangePassword());
        return "main/reset";
    }

    @PostMapping("/password/reset")
    public String resetPassword(@Valid ChangePassword changePassword, HttpSession httpSession, RedirectAttributes redirectAttributes) {
        log.info("In reset password. Request: {}", changePassword);
        return userService.findByUsername(changePassword.getUsername())
                .map(user -> {

                    String password = Util.generateRandomPassword(7);
                    log.info("Generated password: {}", password);
                    String text = "Hi " + user.getFirstName() + ", \nYour password on VMS has been reset. You will be required to change your password on login. " +
                            "Your username is: " + user.getUsername() + " and your password is: " + password + ". " +
                            "\nPlease Make sure you enter a secure password.";
                    user.setPassword(password);
                    user.setIsPasswordChanged(false);
                    redirectAttributes.addFlashAttribute("message", new Message("User reset successful.", MessageType.SUCCESS));


                    user.setIsPasswordChanged(false);
                    userService.update(user);
                    emailService.sendEmail(text, "VMS Login Details", user.getEmail());

                    return "redirect:/";
                }).orElseGet(() -> {
                        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s user not found.", changePassword.getUsername()), MessageType.ERROR));
                        return "redirect:/password/reset";
        });
    }
}
