package com.xelttsh.vms.controllers;

import com.xelttsh.vms.service.DocumentService;
import com.xelttsh.vms.commons.data.DocumentDTO;
import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.enums.EntityName;
import com.xelttsh.vms.model.Document;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/document")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/list")
    public String home(Model model) {
        List<Document> documents = documentService.findAll();
        model.addAttribute("document", documents);
        return "document/list";
    }

    @GetMapping("/create")
    public String renderCreateAgent(Model model) {
        model.addAttribute("document", new Document());
        return "document/create";
    }

    @PostMapping("/create")
    public String create(@RequestParam("file") MultipartFile file,
                         @RequestParam("description") String description,
                         @RequestParam("entityName") String entityName,
                         @RequestParam("entityId") String entityId,
                         RedirectAttributes redirectAttributes) {
        log.info("creating document: {}", file.getOriginalFilename());
        DocumentDTO documentDto = new DocumentDTO();
        try {
            documentDto.setFile(file.getBytes());
            documentDto.setFileName(file.getOriginalFilename());
            documentDto.setDescription(description);
            documentDto.setEntityId(entityId);
            documentDto.setEntityName(entityName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        documentService.create(documentDto);
        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s document uploaded successfully", file.getOriginalFilename()), MessageType.SUCCESS));

        EntityName value = EntityName.valueOf(entityName);
        switch (value) {
            case Agent:
                return "redirect:/agent/view/" + documentDto.getEntityId();
            case ServiceProvider:
                return "redirect:/provider/view/" + documentDto.getEntityId();
            default:
                return null;
        }

    }


}
