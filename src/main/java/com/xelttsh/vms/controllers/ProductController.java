package com.xelttsh.vms.controllers;


import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.enums.EntityStatus;
import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.commons.utils.AjaxResponseBody;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.commons.utils.DateUtil;
import com.xelttsh.vms.service.CategoryService;
import com.xelttsh.vms.model.*;
import com.xelttsh.vms.commons.data.ProductSearch;
import com.xelttsh.vms.commons.data.report.GenerateProductPdfReport;
import com.xelttsh.vms.commons.data.report.ProductReportDTO;
import com.xelttsh.vms.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
@RequestMapping(value="/package")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    /*Boiler Valves*/
    @GetMapping("/boiler/search")
    public String renderBoilerSearch(Model model, HttpSession session) {
        model.addAttribute("search", new ProductSearch());
        session.setAttribute(Constants.SESSION_PDF_PRODUCTS, new ArrayList<>());
        return "boiler/search";
    }


    @GetMapping("/boiler/list")
    public String renderBoilerHome(Model model) {
        return "boiler/list";
    }

    @PostMapping("/boiler/search")
    public String searchBoiler(@Valid ProductSearch productSearch, RedirectAttributes redirectAttributes, HttpSession session) {
        if(productSearch.getAlphaNumeric() == null || productSearch.getAlphaNumeric().isEmpty()) {
            redirectAttributes.addFlashAttribute("message", new Message("Search term is empty.", MessageType.ERROR));
            return "redirect:/package/boiler/search";
        }
        List<Product> products = productService.findByAlphaNumericAndSection(productSearch.getAlphaNumeric(), PlantValveType.BOILER);
        if(products == null || products.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", new Message(String.format("%s not found.", productSearch.getAlphaNumeric()), MessageType.ERROR));
            return "redirect:/package/boiler/search";
        }

        session.setAttribute(Constants.SESSION_PDF_PRODUCTS, products);



        long draftProductsCount = products.stream().filter(product -> EntityStatus.DRAFT.equals(product.getStatus())).count();
        long activeProductsCount = products.stream().filter(product -> EntityStatus.ACTIVE.equals(product.getStatus())).count();
        long rejectedProductsCount = products.stream().filter(product -> EntityStatus.DISAPPROVED.equals(product.getStatus())).count();
        long deletedProductsCount = products.stream().filter(product -> EntityStatus.DELETED.equals(product.getStatus())).count();

        redirectAttributes.addFlashAttribute("deletedProductsCount", deletedProductsCount);
        redirectAttributes.addFlashAttribute("rejectedProductsCount", rejectedProductsCount);
        redirectAttributes.addFlashAttribute("activeProductsCount", activeProductsCount);
        redirectAttributes.addFlashAttribute("draftProductsCount", draftProductsCount);
        redirectAttributes.addFlashAttribute("packages", products);

        return "redirect:/package/boiler/list";
    }

    @RequestMapping(value = "/product/pdfreport", method = RequestMethod.GET,
                   produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateTurbineReport(HttpSession session) throws IOException {

        List<Product> products = (List<Product>) session.getAttribute(Constants.SESSION_PDF_PRODUCTS);

        ByteArrayInputStream bis = GenerateProductPdfReport.productReport(parseProductDTO(products));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=productreport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    private static List<ProductReportDTO> parseProductDTO(List<Product> products) {
        List<ProductReportDTO> productReportDTOS = new ArrayList<>();

        products.forEach(product -> {
            ProductReportDTO productReportDTO = new ProductReportDTO();
            productReportDTO.setAlphaNumeric(product.getAlphaNumeric());
            productReportDTO.setInspectionDate(DateUtil.formatDisplayDate(LocalDateTime.now()));
            productReportDTO.setBoilerUnit(0);
            productReportDTO.setPartDescription("");
            productReportDTO.setStockNo(product.getKksNo());
            productReportDTO.setPartQty(product.getValveQuantity());
            productReportDTO.setInspectionDecission("");
            productReportDTO.setComments("");

            productReportDTOS.add(productReportDTO);
        });
        return productReportDTOS;
    }


    @GetMapping("/home")
    public String renderHome(Model model) {

        List<Product> products = productService.getPackages();
        long draftProductsCount = products.stream().filter(product -> EntityStatus.DRAFT.equals(product.getStatus())).count();
        long activeProductsCount = products.stream().filter(product -> EntityStatus.ACTIVE.equals(product.getStatus())).count();
        long rejectedProductsCount = products.stream().filter(product -> EntityStatus.DISAPPROVED.equals(product.getStatus())).count();
        long deletedProductsCount = products.stream().filter(product -> EntityStatus.DELETED.equals(product.getStatus())).count();
        model.addAttribute("deletedProductsCount", deletedProductsCount);
        model.addAttribute("rejectedProductsCount", rejectedProductsCount);
        model.addAttribute("activeProductsCount", activeProductsCount);
        model.addAttribute("draftProductsCount", draftProductsCount);
        model.addAttribute("packages", products);
        return "product/home";
    }

    @GetMapping("/create")
    public String renderCreate(Model model) {

        List<Category> categoryDTOS = categoryService.findAll();
        log.info("In create. The category size is: {}", categoryDTOS.size());
        model.addAttribute("categoryDTOS", categoryDTOS);
        return "product/create";
    }


    @PostMapping("/create")
    public String create(@Valid Product product, RedirectAttributes redirectAttributes) {

//        product.setLocalDateTimeFromStrings();

        log.info("In create. Product details: {}", product);

        Product response = productService.create(product);
        log.info("Response: {}", response);
        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s package created successfully", product.getAlphaNumeric()), MessageType.SUCCESS));

        return "redirect:/package/view?id="+response.getAlphaNumeric();
    }

    @PostMapping("/update")
    public String update(@Valid Product product, RedirectAttributes redirectAttributes) {
        log.info("In update :::: . Product Package: {}", product);
        Product response = productService.update(product);
        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s package updated successfully", product.getAlphaNumeric()), MessageType.SUCCESS));
        return "redirect:/package/view/" + response.getAlphaNumeric();
    }

    @GetMapping("/update/gasket")
    @ResponseBody
    public ResponseEntity<?> updateGasket(@RequestParam String alphaNumeric,
                                          @RequestParam String gasketSize,
                                          @RequestParam String gasketMake,
                                          @RequestParam String gasketStyle) {

        log.info("In updateGasket. Request: alphanumeric. {}, make. {}, size. {}, style. {}", alphaNumeric, gasketMake, gasketSize, gasketStyle);
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getGasket() != null) {
                product.getGasket().setGasketMake(gasketMake);
                product.getGasket().setGasketSize(gasketSize);
                product.getGasket().setGasketStyle(gasketStyle);
            } else {
                Gasket gasket = new Gasket();
                gasket.setGasketStyle(gasketStyle);
                gasket.setGasketSize(gasketSize);
                gasket.setGasketMake(gasketMake);

                product.setGasket(gasket);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/update/staffingbox")
    @ResponseBody
    public ResponseEntity<?> updateStuffingBox(@RequestParam String alphaNumeric,
                                          @RequestParam String stuffingBoxDepth,
                                          @RequestParam String stuffingBoxDiameter) {

        log.info("In updateStuffingBox. Request: alphanumeric. {}, diameter. {}, depth. {}", alphaNumeric, stuffingBoxDiameter, stuffingBoxDepth);
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getStuffingBox() != null) {
                product.getStuffingBox().setStuffingBoxDiameter(stuffingBoxDiameter);
                product.getStuffingBox().setStuffingBoxDepth(stuffingBoxDepth);
            } else {
                StuffingBox stuffingBox = new StuffingBox();
                stuffingBox.setStuffingBoxDepth(stuffingBoxDepth);
                stuffingBox.setStuffingBoxDiameter(stuffingBoxDiameter);

                product.setStuffingBox(stuffingBox);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/spindle")
    @ResponseBody
    public ResponseEntity<?> updateSpindle(@RequestParam String alphaNumeric,
                                          @RequestParam String spindleLength,
                                          @RequestParam String sprindleDiameter) {

        log.info("In updateSpindle. Request: alphanumeric. {}, diameter. {}, length. {}", alphaNumeric, sprindleDiameter, spindleLength);
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getSpindle() != null) {
                product.getSpindle().setSprindleDiameter(sprindleDiameter);
                product.getSpindle().setSpindleLength(spindleLength);
            } else {
                Spindle spindle = new Spindle();
                spindle.setSpindleLength(spindleLength);
                spindle.setSprindleDiameter(sprindleDiameter);

                product.setSpindle(spindle);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/spindlerunout")
    @ResponseBody
    public ResponseEntity<?> updateSpindleRunOut(@RequestParam String alphaNumeric,
                                          @RequestParam String sprindleRunOutBushID,
                                          @RequestParam String sprindleRunOutBackStop,
                                          @RequestParam String sprindleRunOutBushOD) {

        log.info("In updateSpindleRunOut. ");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getSpindleRunOut() != null) {
                product.getSpindleRunOut().setSprindleRunOutBushOD(sprindleRunOutBushOD);
                product.getSpindleRunOut().setSprindleRunOutBushID(sprindleRunOutBushID);
                product.getSpindleRunOut().setSprindleRunOutBackStop(sprindleRunOutBackStop);
            } else {
                SpindleRunOut spindleRunOut = new SpindleRunOut();
                spindleRunOut.setSprindleRunOutBushID(sprindleRunOutBushID);
                spindleRunOut.setSprindleRunOutBackStop(sprindleRunOutBackStop);
                spindleRunOut.setSprindleRunOutBushOD(sprindleRunOutBushOD);

                product.setSpindleRunOut(spindleRunOut);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/pressuregasket")
    @ResponseBody
    public ResponseEntity<?> updatePressureGasket(@RequestParam String alphaNumeric,
                                          @RequestParam String pressureGasketOD,
                                          @RequestParam String pressureGasketID,
                                          @RequestParam String pressureGasketExternalTaper,
                                          @RequestParam String pressureGasketInternalTaper) {

        log.info("In updatePressureGasket. ");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getPressureGasket() != null) {
                product.getPressureGasket().setPressureGasketInternalTaper(pressureGasketInternalTaper);
                product.getPressureGasket().setPressureGasketOD(pressureGasketOD);
                product.getPressureGasket().setPressureGasketID(pressureGasketID);
                product.getPressureGasket().setPressureGasketExternalTaper(pressureGasketExternalTaper);
            } else {
                PressureGasket pressureGasket = new PressureGasket();
                pressureGasket.setPressureGasketOD(pressureGasketOD);
                pressureGasket.setPressureGasketID(pressureGasketID);
                pressureGasket.setPressureGasketInternalTaper(pressureGasketInternalTaper);
                pressureGasket.setPressureGasketExternalTaper(pressureGasketExternalTaper);

                product.setPressureGasket(pressureGasket);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/glandpackaging")
    @ResponseBody
    public ResponseEntity<?> updateGlandPackaging(@RequestParam String alphaNumeric,
                                          @RequestParam String glandPackagingSize,
                                          @RequestParam String glandPackagingStyle,
                                          @RequestParam String glandPackagingMake) {

        log.info("In updateGlandPackaging. ");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            if(product.getGlandPackaging() != null) {
                product.getGlandPackaging().setGlandPackagingSize(glandPackagingSize);
                product.getGlandPackaging().setGlandPackagingStyle(glandPackagingStyle);
                product.getGlandPackaging().setGlandPackagingMake(glandPackagingMake);
            } else {
                GlandPackaging glandPackaging = new GlandPackaging();
                glandPackaging.setGlandPackagingSize(glandPackagingSize);
                glandPackaging.setGlandPackagingStyle(glandPackagingStyle);
                glandPackaging.setGlandPackagingMake(glandPackagingMake);

                product.setGlandPackaging(glandPackaging);
            }

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/general")
    @ResponseBody
    public ResponseEntity<?> updateGeneral(@RequestParam String alphaNumeric,
                                          @RequestParam String kksNo,
                                          @RequestParam String system,
                                          @RequestParam String service,
                                          @RequestParam String drawingNumber,
                                          @RequestParam String pn,
                                          @RequestParam BigDecimal nb) {

        log.info("In updateGeneral.");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            product.setKksNo(kksNo);
            product.setSystem(system);
            product.setService(service);
            product.setDrawingNumber(drawingNumber);
            product.setPn(pn);
            product.setNb(nb);

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/specific")
    @ResponseBody
    public ResponseEntity<?> updateSpecific(@RequestParam String alphaNumeric,
                                          @RequestParam String designPressure,
                                          @RequestParam String designTemperature,
                                          @RequestParam String material,
                                          @RequestParam String drive,
                                          @RequestParam String connections,
                                          @RequestParam String make,
                                          @RequestParam String sow,
                                          @RequestParam Integer valveQuantity) {

        log.info("In updateSpecific.");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            product.setDesignPressure(designPressure);
            product.setDesignTemperature(designTemperature);
            product.setMaterial(material);
            product.setDrive(drive);
            product.setConnections(connections);
            product.setMake(make);
            product.setSow(sow);
            product.setValveQuantity(valveQuantity);

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/update/summation")
    @ResponseBody
    public ResponseEntity<?> updateSummation(@RequestParam String alphaNumeric,
                                          @RequestParam BigDecimal rate,
                                          @RequestParam BigDecimal amount,
                                          @RequestParam BigDecimal subTotals,
                                          @RequestParam String comments,
                                          @RequestParam String description) {

        log.info("In updateSummation.");
        AjaxResponseBody result = new AjaxResponseBody();

        Product product = productService.findById(alphaNumeric);
        if(product != null) {
            product.setRate(rate);
            product.setAmount(amount);
//            product.setSubTotals(subTotals);
            product.setComments(comments);
            product.setDescription(description);

            productService.update(product);
            result.setCode("00");
            result.setMsg("Successful.");
        } else {
            result.setCode("05");
            result.setMsg("Product not found.");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/count/today/created/turbines")
    @ResponseBody
    public ResponseEntity<?> getTodayTurbines() {
        List<Product> products = productService.findProductsByCategory_PlantValveTypeAndDate(PlantValveType.TURBINE, LocalDateTime.now().withHour(0).withMinute(0).withSecond(0), LocalDateTime.now().withHour(23).withMinute(59).withSecond(59));
        int size = products.size();

        AjaxResponseBody result = new AjaxResponseBody();
        result.setCode("00");
        result.setMsg("Successful.");
        result.setBody(size);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/count/yesterday/created/turbines")
    @ResponseBody
    public ResponseEntity<?> getYesterdayTurbines() {
        List<Product> products = productService.findProductsByCategory_PlantValveTypeAndDate(PlantValveType.TURBINE, LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).minusDays(1), LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).minusDays(1));
        int size = products.size();

        AjaxResponseBody result = new AjaxResponseBody();
        result.setCode("00");
        result.setMsg("Successful.");
        result.setBody(size);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/count/today/created/boilers")
    @ResponseBody
    public ResponseEntity<?> getTodayBoilers() {
        List<Product> products = productService.findProductsByCategory_PlantValveTypeAndDate(PlantValveType.BOILER, LocalDateTime.now().withHour(0).withMinute(0).withSecond(0), LocalDateTime.now().withHour(23).withMinute(59).withSecond(59));
        int size = products.size();

        AjaxResponseBody result = new AjaxResponseBody();
        result.setCode("00");
        result.setMsg("Successful.");
        result.setBody(size);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/count/yesterday/created/boilers")
    @ResponseBody
    public ResponseEntity<?> getYesterdayBoilers() {
        List<Product> products = productService.findProductsByCategory_PlantValveTypeAndDate(PlantValveType.BOILER, LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).minusDays(1), LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).minusDays(1));
        int size = products.size();

        AjaxResponseBody result = new AjaxResponseBody();
        result.setCode("00");
        result.setMsg("Successful.");
        result.setBody(size);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/approve")
    public String approve(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In approve. Product" );
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.ACTIVE);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s product approved successfully", id), MessageType.SUCCESS));
        return "redirect:/package/view?id=" + id;
    }

    @GetMapping("/reject")
    public String reject(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In reject. Product" );
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.DISAPPROVED);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s product disapproved successfully", id), MessageType.SUCCESS));
        return "redirect:/package/view?id=" + id;
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In delete. Product" );
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.DELETED);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s package deleted successfully", id), MessageType.SUCCESS));
        return "redirect:/package/view?id=" + id;
    }


    @GetMapping("/view")
    public String view(@RequestParam("id") String id, Model model) {
        Product product = productService.findById(id);
        log.info("In view. Product Package: {}", product);

//        List<PackageBenefit> benefits = packageBenefitService
//                .findAllByProductPackage_ProductPackageId(product.getAlphaNumeric());

        model.addAttribute("package", product);
        return "product/view";
    }




}
