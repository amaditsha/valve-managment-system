package com.xelttsh.vms.controllers;

import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.data.ProductSearch;
import com.xelttsh.vms.commons.enums.EntityStatus;
import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.model.Category;
import com.xelttsh.vms.model.Product;
import com.xelttsh.vms.service.CategoryService;
import com.xelttsh.vms.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Controller
@RequestMapping("/boiler")
public class BoilerController {
    private final ProductService productService;
    private final CategoryService categoryService;

    @GetMapping("/home")
    public String renderHome(Model model) {

        List<Product> products = productService.findAllByCategory_ValveType(PlantValveType.BOILER);
        long draftProductsCount = products.stream().filter(product -> EntityStatus.DRAFT.equals(product.getStatus())).count();
        long activeProductsCount = products.stream().filter(product -> EntityStatus.ACTIVE.equals(product.getStatus())).count();
        long rejectedProductsCount = products.stream().filter(product -> EntityStatus.DISAPPROVED.equals(product.getStatus())).count();
        long deletedProductsCount = products.stream().filter(product -> EntityStatus.DELETED.equals(product.getStatus())).count();
        model.addAttribute("deletedProductsCount", deletedProductsCount);
        model.addAttribute("rejectedProductsCount", rejectedProductsCount);
        model.addAttribute("activeProductsCount", activeProductsCount);
        model.addAttribute("draftProductsCount", draftProductsCount);
        model.addAttribute("packages", products);
        return "boiler/home";
    }

    @GetMapping("/create")
    public String createTurbine(Model model) {
        List<Category> categoryDTOS = categoryService.findAllByPlantValveType(PlantValveType.BOILER);
        model.addAttribute("categoryDTOS", categoryDTOS);
        model.addAttribute("boiler", new Product());
        return "boiler/create";
    }

    @PostMapping("/create")
    public String create(@Valid Product product, RedirectAttributes redirectAttributes) {
        log.info("In create. Boiler: {}", product);

        Product response = productService.create(product);
        log.info("Response: {}", response);
        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s boiler created successfully", product.getAlphaNumeric()), MessageType.SUCCESS));

        return "redirect:/boiler/view?id="+response.getAlphaNumeric();
    }

    @GetMapping("/view")
    public String view(@RequestParam("id") String id, Model model) {
        Product product = productService.findById(id);
        log.info("In view. Boiler: {}", product);

        model.addAttribute("package", product);
        return "boiler/view";
    }

    @GetMapping("/approve")
    public String approve(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In approve. Boiler id: " + id );
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.ACTIVE);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s approved successfully", id), MessageType.SUCCESS));
        return "redirect:/boiler/view?id=" + id;
    }

    @GetMapping("/reject")
    public String reject(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In reject. Boiler id: " + id);
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.DISAPPROVED);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s disapproved successfully", id), MessageType.SUCCESS));
        return "redirect:/boiler/view?id=" + id;
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") String id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In delete. Boiler id: " + id );
        Product product = productService.findById(id);
        if(product != null) {
            product.setStatus(EntityStatus.DELETED);
            productService.update(product);
        }

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s deleted successfully", id), MessageType.SUCCESS));
        return "redirect:/boiler/view?id=" + id;
    }


    @GetMapping("/search")
    public String renderTurbineSearch(Model model, HttpSession session) {
        model.addAttribute("search", new ProductSearch());
        session.setAttribute(Constants.SESSION_PDF_PRODUCTS, new ArrayList<>());
        return "boiler/search";
    }


    @PostMapping("/search")
    public String searchTurbine(@Valid ProductSearch productSearch, RedirectAttributes redirectAttributes, HttpSession session) {
        log.info("In search: Request: {}", productSearch);
        List<Product> products = productService.search(productSearch);
        if(products == null || products.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", new Message(String.format("%s not found.", productSearch.getAlphaNumeric()), MessageType.ERROR));
            return "redirect:/boiler/search";
        }

//        model.addAttribute("search", new ProductSearch());

        session.setAttribute(Constants.SESSION_PDF_PRODUCTS, products);
        log.info("Results: {}", products.size());



        long draftProductsCount = products.stream().filter(product -> EntityStatus.DRAFT.equals(product.getStatus())).count();
        long activeProductsCount = products.stream().filter(product -> EntityStatus.ACTIVE.equals(product.getStatus())).count();
        long rejectedProductsCount = products.stream().filter(product -> EntityStatus.DISAPPROVED.equals(product.getStatus())).count();
        long deletedProductsCount = products.stream().filter(product -> EntityStatus.DELETED.equals(product.getStatus())).count();

        redirectAttributes.addFlashAttribute("deletedProductsCount", deletedProductsCount);
        redirectAttributes.addFlashAttribute("rejectedProductsCount", rejectedProductsCount);
        redirectAttributes.addFlashAttribute("activeProductsCount", activeProductsCount);
        redirectAttributes.addFlashAttribute("draftProductsCount", draftProductsCount);
        redirectAttributes.addFlashAttribute("packages", products);

        return "redirect:/boiler/list";
    }

    @GetMapping("/list")
    public String renderTurbineHome(Model model) {
//        model.addAttribute("packages", products);
        return "boiler/list";
    }



    @GetMapping("/sections")
    public String viewSections(Model model) {
        List<Category> categories = categoryService.findAllByPlantValveType(PlantValveType.BOILER);
        model.addAttribute("categories", categories);
        return "boiler/section/home";
    }

    @GetMapping("/section/create")
    public String renderCreate() {
        return "boiler/section/create";
    }

    @PostMapping("/section/create")
    public String create(@Valid Category category) {
        log.info("In create. Section details: {}", category);

        Category response = categoryService.create(category);
        log.info("Response: {}", response);
        return "redirect:/boiler/section/view/"+response.getCategoryId();

    }

//

    @GetMapping("/section/update/{id}")
    public String renderUpdate(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.findById(id);
        log.info("In update. Section: {}", category);
        model.addAttribute("category", category);
        return "boiler/section/update";
    }

    @PostMapping("/section/update")
    public String update(@Valid Category category) {
        log.info("In update :::: . Section: {}", category);
        Category response = categoryService.update(category);
        return "redirect:/boiler/section/view/" + response.getCategoryId();
    }

    @GetMapping("/section/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In delete. Section id: {}", id);
        Category deletedCategory = categoryService.delete(id);
        log.info("In delete. Product Category: {}", deletedCategory);
        model.addAttribute("deletedCategory", deletedCategory);

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s section deleted successfully", deletedCategory.getName()), MessageType.SUCCESS));
        return "redirect:/boiler/sections";
    }

    @GetMapping("/section/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.findById(id);
        log.info("In view. Section: {}", category);

        List<Product> packages = productService
                .findAllByCategory_CategoryId(category.getCategoryId());


        model.addAttribute("category", category);
        model.addAttribute("packages", packages);
        return "boiler/section/view";
    }


}
