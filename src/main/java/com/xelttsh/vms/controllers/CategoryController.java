package com.xelttsh.vms.controllers;


import com.xelttsh.vms.service.CategoryService;
import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.service.ProductService;
import com.xelttsh.vms.model.Category;
import com.xelttsh.vms.model.Product;
//import com.xelttsh.vms.exceptions.BusinessException;
//import com.xelttsh.vms.exceptions.RecordNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@Slf4j
@RequestMapping(value = "/category")
public class CategoryController {

@Autowired
    private CategoryService categoryService;

@Autowired
private ProductService productService;

    @GetMapping("/home")
    public String renderHome(Model model) {

        List<Category> productCategories = categoryService.findAll();
        model.addAttribute("categories", productCategories);
        return "category/home";
    }

    @GetMapping("/create")
    public String renderCreate() {

        return "category/create";
    }

    @PostMapping("/create")
    public String create(@Valid Category category) {
        log.info("In create. Product Category details: {}", category);

        Category response = categoryService.create(category);
        log.info("Response: {}", response);
        return "redirect:/category/view/"+response.getCategoryId();

    }

//

    @GetMapping("/update/{id}")
    public String renderUpdate(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.findById(id);
        log.info("In view. Product Category: {}", category);

        model.addAttribute("category", category);
        return "category/update";
    }

    @PostMapping("/update")
    public String update(@Valid Category category) {
        log.info("In update :::: . Product Category: {}", category);
        Category response = categoryService.update(category);
        return "redirect:/category/view/" + response.getCategoryId();
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        log.info("In delete. Product Category: ----" );
        Category deletedCategory = categoryService.delete(id);
        log.info("In delete. Product Category: {}", deletedCategory);
        model.addAttribute("deletedCategory", deletedCategory);

        redirectAttributes.addFlashAttribute("message", new Message(String.format("%s category deleted successfully", deletedCategory.getName()), MessageType.SUCCESS));
        return "redirect:/category/home";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.findById(id);
        log.info("In view. Product Category: {}", category);

        List<Product> packages = productService
                .findAllByCategory_CategoryId(category.getCategoryId());


        model.addAttribute("category", category);
        model.addAttribute("packages", packages);
        return "category/view";
    }

    private void init(Category category, Model model) {
        model.addAttribute("productCategory", category);
    }



}
