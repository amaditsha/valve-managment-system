package com.xelttsh.vms.controllers;

import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.enums.Status;
import com.xelttsh.vms.commons.exceptions.BusinessException;
import com.xelttsh.vms.commons.exceptions.RecordNotFoundException;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.commons.utils.Util;
import com.xelttsh.vms.mail.EmailService;
import com.xelttsh.vms.model.User;
import com.xelttsh.vms.service.RoleService;
import com.xelttsh.vms.service.UserService;
import com.xelttsh.vms.commons.data.UserSearch;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
@Slf4j
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final EmailService emailService;

    @GetMapping("/list")
    public String list(Model model) {
        List<User> users = userService.findAll().stream().filter(user -> !Status.DELETED.equals(user.getStatus())).collect(Collectors.toList());
        int noOfUsers = users != null ? users.size() : 0 ;
        model.addAttribute("users", users);
        model.addAttribute("message", new Message(noOfUsers + " user(s) found.", MessageType.INFO));
        return "user/list";
    }

    @GetMapping("/create")
    public String createForm(Model model) {
        this.init(new User(), model);
        return "user/create";
    }

    @PostMapping("/create")
    public String create(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, HttpSession httpSession) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "user/create";
        }

        String loggedInUsername = (String)httpSession.getAttribute(Constants.SESSION_USERNAME);

        user.setCreatedBy(Optional.ofNullable(loggedInUsername).orElse("system"));
        user.setStatus(Status.PENDING_ACTIVATION);

        user = userService.create(user).orElseThrow(() -> new BusinessException("Could not create user"));
        redirectAttributes.addFlashAttribute("message", new Message("User created successfully.", MessageType.SUCCESS));
        return "redirect:/user/view/"+ user.getUsername();
    }

    @GetMapping("/search")
    public String searchForm(Model model) {
        model.addAttribute("user", new UserSearch());
        return "user/search";
    }

    @PostMapping("/search")
    public String search(@Valid UserSearch userSearch, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("user", userSearch);
            return "user/search";
        }
        Optional<User> optionalUser = userService.findByUsername(userSearch.getUsername());

        if (!optionalUser.isPresent()) {
            model.addAttribute("user", userSearch);
            model.addAttribute("message", new Message("User with username [" + userSearch.getUsername() + "] not found.", MessageType.ERROR));
            return "user/search";
        }
        model.addAttribute("user", optionalUser.get());
        return "user/view";
    }

    @GetMapping("/view/{username}")
    public String view(@PathVariable("username") String username, Model model) {
        User user = userService.findByUsername(username).orElseThrow(() -> new RecordNotFoundException("User not found"));
        model.addAttribute("user", user);
        return "user/view";
    }



    @GetMapping("/activate/{username}")
    public String activate(@PathVariable("username") String username, RedirectAttributes redirectAttributes) {
        userService.findByUsername(username)
        .map(user -> {
            user.setStatus(Status.ACTIVE);
            String password = Util.generateRandomPassword(7);
            log.info("Generated password: {}", password);
            String text = "Hi " + user.getFirstName() + ", \nYour profile on VMS has been approved. You will be required to change your password on initial login. " +
                    "Your username is: " + user.getUsername() + " and your password is: " + password + ". " +
                    "\nPlease Make sure you enter a secure password.";
            user.setPassword(password);
            redirectAttributes.addFlashAttribute("message", new Message("User activated successfully.", MessageType.SUCCESS));

            emailService.sendEmail(text, "VMS Login Details", user.getEmail());
            return userService.update(user);
        });
        return "redirect:/user/view/" + username;
    }



    @GetMapping("/reset/{username}")
    public String reset(@PathVariable("username") String username, RedirectAttributes redirectAttributes) {
        userService.findByUsername(username)
        .map(user -> {
            String password = Util.generateRandomPassword(7);
            log.info("Generated password: {}", password);
            String text = "Hi " + user.getFirstName() + ", \nYour password on VMS has been reset. You will be required to change your password on login. " +
                    "Your username is: " + user.getUsername() + " and your password is: " + password + ". " +
                    "\nPlease Make sure you enter a secure password.";
            user.setPassword(password);
            user.setIsPasswordChanged(false);
            redirectAttributes.addFlashAttribute("message", new Message("User reset successful.", MessageType.SUCCESS));

            emailService.sendEmail(text, "VMS Login Details", user.getEmail());
            return userService.update(user);
        });
        return "redirect:/user/view/" + username;
    }

    @GetMapping("/deactivate/{username}")
    public String deactivate(@PathVariable("username") String username, RedirectAttributes redirectAttributes) {
        userService.findByUsername(username)
        .map(user -> {
            user.setStatus(Status.INACTIVE);
            redirectAttributes.addFlashAttribute("message", new Message("User de-activated successfully.", MessageType.SUCCESS));

            return userService.update(user);
        });
        return "redirect:/user/view/" + username;
    }

    @GetMapping("/delete/{username}")
    public String delete(@PathVariable("username") String username, RedirectAttributes redirectAttributes) {
        userService.findByUsername(username)
        .map(user -> {
            user.setStatus(Status.DELETED);
            redirectAttributes.addFlashAttribute("message", new Message("User deleted successfully.", MessageType.SUCCESS));

            return userService.update(user);
        });
        return "redirect:/user/view/" + username;
    }

    @GetMapping("/update/{username}")
    public String updateForm(@PathVariable("username") String username, Model model) {
        User user = userService.findByUsername(username).orElseThrow(() -> new RecordNotFoundException("User not found."));
        this.init(user, model);
        return "user/update";
    }

    @PostMapping("/update")
    public String update(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        if(bindingResult.hasErrors()) {
            this.init(user, model);
            return "user/update";
        }

        user = userService.update(user).orElseThrow(() -> new BusinessException("Could not update user"));
        redirectAttributes.addFlashAttribute("message", new Message("User updated successfully.", MessageType.SUCCESS));
        return "redirect:/user/view/"+ user.getUsername();
    }

    private void init(User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("roles", roleService.findAll());
    }


}
