package com.xelttsh.vms.controllers;

import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.model.Parameter;
import com.xelttsh.vms.service.ParameterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/parameter")
@Slf4j
public class ParameterController {

    @Autowired
    private ParameterService parameterService;

    @GetMapping
    public String home(Model model) {
        model.addAttribute("parameter", parameterService.findAll());
        return "parameter/list";
    }

    @GetMapping("/create")
    public String renderCreate(Model model) {
        model.addAttribute("parameter", new Parameter());
        return "parameter/create";
    }

    @PostMapping("/create")
    public String create(Parameter parameter, RedirectAttributes redirectAttributes) {
        parameterService.create(parameter);
        redirectAttributes.addFlashAttribute( "message", new Message(String.format("%s parameter created successfully", parameter.getKey()), MessageType.SUCCESS));
        return "redirect:/parameter";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Parameter parameter = parameterService.findById(id);
        log.info("In view. Parameter: {}", parameter);
        model.addAttribute("parameter", parameter);
        return "parameter/view";
    }

    @GetMapping("/update/{id}")
    public String renderUpdate(@PathVariable("id") Long id, Model model) {
        Parameter parameter = parameterService.findById(id);
        log.info("In view. Parameter: {}", parameter);
        model.addAttribute("parameter", parameter);
        return "parameter/update";
    }

    @PostMapping("/update")
    public String update(@Valid Parameter parameter, RedirectAttributes redirectAttributes) {
        log.info("In update :::: . Parameter: {}", parameter);
        Parameter response = parameterService.update(parameter);
        redirectAttributes.addFlashAttribute( "message", new Message(String.format("%s parameter updated successfully", parameter.getKey()), MessageType.SUCCESS));
        return "redirect:/parameter/view/" + response.getParameterId();
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        log.info("parameter to be deleted: {}", id);

        Parameter parameter = parameterService.findById(id);
        parameterService.delete(id);

        redirectAttributes.addFlashAttribute( "message", new Message(String.format("%s parameter deleted successfully", parameter.getKey()), MessageType.SUCCESS));
        return "redirect:/parameter";
    }
}
