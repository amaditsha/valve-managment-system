package com.xelttsh.vms.controllers;

import com.xelttsh.vms.commons.Message;
import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.enums.security.AccessRight;
import com.xelttsh.vms.commons.exceptions.BusinessException;
import com.xelttsh.vms.commons.exceptions.RecordNotFoundException;
import com.xelttsh.vms.commons.utils.Constants;
import com.xelttsh.vms.model.Role;
import com.xelttsh.vms.commons.data.RoleSearch;
import com.xelttsh.vms.service.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
@RequestMapping("/role")
@Slf4j
public class RoleController {
    private final RoleService roleService;

    @GetMapping("/list")
    public String list(Model model) {
        List<Role> roles = roleService.findAll();
        int noOfRoles = roles != null ? roles.size() : 0 ;
        model.addAttribute("roles", roles);
        model.addAttribute("message", new Message(noOfRoles + " role(s) found.", MessageType.INFO));
        return "role/list";
    }

    @GetMapping("/create")
    public String createForm(Model model) {
        model.addAttribute("role", new Role());
        return "role/create";
    }

    @PostMapping("/create")
    public String create(@Valid Role role, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, HttpSession httpSession) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("role", role);
            return "user/create";
        }

        role.setCreatedBy((String)httpSession.getAttribute(Constants.SESSION_USERNAME));


        role = roleService.create(role).orElseThrow(() -> new BusinessException("Could not create role"));
        redirectAttributes.addFlashAttribute("message", new Message("Role created successfully.", MessageType.SUCCESS));
        return "redirect:/role/view/"+ role.getId();
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Role role = roleService.findById(id).orElseThrow(() -> new RecordNotFoundException("Role not found."));
        model.addAttribute("role", role);
        return "role/view";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        roleService.findById(id)
                .map(role -> {
                    roleService.delete(role);
                    return "";
                });
        redirectAttributes.addFlashAttribute("message", new Message("Role deleted successfully.", MessageType.SUCCESS));
        return "redirect:/role/list";
    }

    @GetMapping("/remove/{accessright}/{id}")
    @ResponseBody
    public String removeAccessRight(@PathVariable("accessright") int accessright, @PathVariable("id") Long id) {
        AccessRight accessRight = AccessRight.values()[accessright];
        log.info("In removeAccessRight. Access right: {} and role id: {}", accessright, id);
        Optional<String> stringOptional = roleService.findById(id)
                .map(role -> {
                    role.remove(accessRight);
                    roleService.update(role);
                    return "";
                });
        return stringOptional.orElse(null);
    }

    @GetMapping("/add/{accessright}/{id}")
    @ResponseBody
    public String addAccessRight(@PathVariable("accessright") int accessright, @PathVariable("id") Long id) {
        AccessRight accessRight = AccessRight.values()[accessright];
        log.info("In addAccessRight. Access right: {} and role id: {}", accessright, id);
        Optional<String> stringOptional = roleService.findById(id)
                .map(role -> {
                    role.add(accessRight);
                    roleService.update(role);
                    return "";
                });
        return stringOptional.orElse(null);
    }

    @GetMapping("/search")
    public String searchForm(Model model) {
        model.addAttribute("role", new RoleSearch());
        return "role/search";
    }

    @PostMapping("/search")
    public String search(@Valid RoleSearch roleSearch, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        if(bindingResult.hasErrors()) {
            model.addAttribute("role", roleSearch);
            return "role/search";
        }
        Optional<Role> optionalRole = roleService.findByName(roleSearch.getName());
        if (!optionalRole.isPresent()) {
            model.addAttribute("role", roleSearch);
            model.addAttribute("message", new Message("Role with name [" + roleSearch.getName() + "] not found.", MessageType.ERROR));
            return "role/search";
        }
        model.addAttribute("role", optionalRole.get());
        return "role/view";
    }


}
