package com.xelttsh.vms.repository;

import com.xelttsh.vms.model.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParameterRepository extends JpaRepository<Parameter, Long> {
}
