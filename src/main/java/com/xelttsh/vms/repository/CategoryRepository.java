package com.xelttsh.vms.repository;

import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findAllByPlantValveType(PlantValveType type);
}
