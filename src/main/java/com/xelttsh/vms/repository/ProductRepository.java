package com.xelttsh.vms.repository;

import com.xelttsh.vms.commons.enums.EntityStatus;
import com.xelttsh.vms.commons.enums.PlantValveType;
import com.xelttsh.vms.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findProductByCategory_CategoryId(Long productCategoryId);
    List<Product> findProductByCategory_PlantValveType(PlantValveType type);
    @Query("SELECT p FROM Product p WHERE p.category.plantValveType =:type AND p.dateCreated >:startTime AND p.dateCreated <:endTime")
    List<Product> findProductsByCategory_PlantValveTypeAndDate(@Param("type") PlantValveType type, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
    @Query("SELECT p FROM Product p where p.alphaNumeric = :alphaNumeric AND p.category.plantValveType = :section AND p.status <> :status" )
    List<Product> findByAlphaNumericAndSection(@Param("alphaNumeric") String alphaNumeric, @Param("section") PlantValveType section, @Param("status") EntityStatus status);
    @Query("SELECT p FROM Product p WHERE (:alphaNumeric IS NULL OR p.alphaNumeric =:alphaNumeric) AND (:valveType IS NULL OR p.valveType =:valveType) AND (:kksNo IS NULL OR p.kksNo =:kksNo) AND (:service IS NULL OR p.service =:service) AND (:dateFrom IS NULL OR p.dateCreated >:dateFrom) AND (:dateTo IS NULL OR p.dateCreated <:dateTo) AND (:type IS NULL OR p.category.plantValveType =:type)")
    List<Product> search(@Param("alphaNumeric") String alphaNumeric, @Param("valveType") String valveType, @Param("kksNo") String kksNo, @Param("service") String service, @Param("dateFrom") LocalDateTime dateFrom, @Param("dateTo") LocalDateTime dateTo, @Param("type") PlantValveType type);
}
