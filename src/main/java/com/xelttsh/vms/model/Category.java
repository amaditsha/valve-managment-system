package com.xelttsh.vms.model;

import com.xelttsh.vms.commons.enums.PlantValveType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long categoryId;
    @Column(nullable = false, length = 150)
    private String name;
    @Column(length = 225)
    private String description;
    @Column
    private LocalDateTime dateCreated;
    @OneToMany(targetEntity = Product.class)
    private List<Product> products;
    @Column
    private Boolean active;
    @Column
    @Enumerated(EnumType.STRING)
    private PlantValveType plantValveType;

    @PrePersist
    public  void init() {
        setDateCreated(LocalDateTime.now());
    }

}
