package com.xelttsh.vms.model;

import com.xelttsh.vms.commons.enums.Gender;
import com.xelttsh.vms.commons.enums.Status;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Audited
@Entity
@Data
@Table(name = "users")
public class User extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "is_password_changed")
    private Boolean isPasswordChanged;

    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;

    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;

    @Basic(optional = false)
    @Column(name = "dob")
    private LocalDate dob;

    @Basic(optional = false)
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "is_ecocash_customer")
    private boolean ecocashCustomer = true;

    @Column(name = "activation_code")
    private String activationCode;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "first_log_in")
    private boolean firstLogIn = true;

    @JoinColumn(name = "role_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Role role;

}

