package com.xelttsh.vms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Gasket {
    private String gasketSize;

    private String gasketStyle;

    private String gasketMake;
}
