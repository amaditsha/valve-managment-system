package com.xelttsh.vms.model;

import com.xelttsh.vms.commons.enums.EntityName;
import com.xelttsh.vms.commons.enums.EntityStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Document implements Serializable {
    private Long id;
    private LocalDateTime dateCreated;
    private EntityStatus status;
    private String name;
    private String description;
    private String filePath;
    private EntityName entityName;
    private String entityId;
}
