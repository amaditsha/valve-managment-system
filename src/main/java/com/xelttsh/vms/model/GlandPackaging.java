package com.xelttsh.vms.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class GlandPackaging {
    private String glandPackagingSize;

    private String glandPackagingStyle;

    private String glandPackagingMake;
}
