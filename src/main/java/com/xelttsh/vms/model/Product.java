package com.xelttsh.vms.model;

import com.xelttsh.vms.commons.enums.EntityStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Product implements Serializable {


    @Id
    private String alphaNumeric;

    private String kksNo;

    @Column(name = "productSystem")
    private String system;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Category category;

    private transient Long categoryId;

    private String service;

    private String drawingNumber;

    private String valveType;

    private BigDecimal nb;

    private String pn;

    private String designTemperature;

    private String designPressure;

    private String material;

    private String drive;

    private String connections;

    private String typeNumber;

    private String make;

    private String sow;

    private Integer valveQuantity;

    @Embedded
    private Gasket gasket;

    @Embedded
    private StuffingBox stuffingBox;

    @Embedded
    private Spindle spindle;

    @Embedded
    private SpindleRunOut spindleRunOut;

    @Embedded
    private PressureGasket pressureGasket;

    @Embedded
    private GlandPackaging glandPackaging;

    private BigDecimal rate;

    private BigDecimal amount;

    private BigDecimal subTotals;

    private String comments;

    private String description;

    @Enumerated(EnumType.STRING)
    private EntityStatus status;

    private LocalDateTime dateCreated;

    @PrePersist
    public void init() {
        this.setDateCreated(LocalDateTime.now());
        this.setStatus(EntityStatus.DRAFT);
    }


}


