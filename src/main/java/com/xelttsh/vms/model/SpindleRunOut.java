package com.xelttsh.vms.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class SpindleRunOut {
    private String sprindleRunOutBushID;

    private String sprindleRunOutBushOD;

    private String sprindleRunOutBackStop;
}
