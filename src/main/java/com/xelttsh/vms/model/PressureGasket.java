package com.xelttsh.vms.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class PressureGasket {
    private String pressureGasketOD;

    private String pressureGasketID;

    private String pressureGasketInternalTaper;

    private String pressureGasketExternalTaper;
}
