package com.xelttsh.vms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StuffingBox {
    private String stuffingBoxDepth;

    private String stuffingBoxDiameter;
}
