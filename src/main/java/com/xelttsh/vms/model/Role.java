package com.xelttsh.vms.model;

import com.xelttsh.vms.commons.enums.security.AccessRight;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Audited
@Data
@Table(name = "role")
public class Role extends AbstractAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_role_limited")
    private Boolean roleLimited;

    @Column(name = "description")
    private String description;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "role_access_right", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "access_right", nullable = false)
    @Enumerated(EnumType.STRING)
    Collection<AccessRight> accessRights;

    public void remove(AccessRight accessRight) {
        if(this.accessRights == null) {
            this.accessRights = new ArrayList<>();
        }

        this.accessRights.remove(accessRight);
    }

    public void add(AccessRight accessRight) {
        if(this.accessRights == null) {
            this.accessRights = new ArrayList<>();
        }

        this.accessRights.add(accessRight);
    }

}
