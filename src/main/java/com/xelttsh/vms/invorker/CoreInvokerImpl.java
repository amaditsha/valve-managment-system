package com.xelttsh.vms.invorker;

import com.xelttsh.vms.commons.ApiHeader;
import com.xelttsh.vms.commons.ResponseHandler;
import com.xelttsh.vms.commons.data.ApiResponse;
import com.xelttsh.vms.commons.exceptions.BusinessException;
import com.xelttsh.vms.commons.exceptions.ConnectionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
@Slf4j
public class CoreInvokerImpl implements CoreInvoker {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ApiHeader apiHeader;


    @Override
    public <T, R> R invoke(T t, String resourceUrl, HttpMethod httpMethod, ParameterizedTypeReference parameterizedTypeReference) {
        try {

            RequestEntity<T> requestEntity = null;
            if(t == null) {
                requestEntity = new RequestEntity<>(apiHeader.headerWithToken(),
                        httpMethod, new URI(resourceUrl));
            }else{
                requestEntity = new RequestEntity<>(t, apiHeader.headerWithToken(),
                        httpMethod, new URI(resourceUrl));
            }
            restTemplate.setErrorHandler(new ResponseHandler());
            final ResponseEntity<ApiResponse<R>> responseEntity
                    = restTemplate.exchange(requestEntity, parameterizedTypeReference);

            final ApiResponse<R> apiResponse = responseEntity.getBody();
            if(apiResponse.getStatus() == HttpStatus.OK.value()){
                return apiResponse.getBody();
            }
            throw new BusinessException(apiResponse.getStatus(),apiResponse.getMessage());
        } catch (URISyntaxException e) {
            throw new ConnectionException(e);
        }
    }

}
