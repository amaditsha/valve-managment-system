package com.xelttsh.vms.invorker;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;

public interface CoreInvoker {

    <T, R> R invoke(T t, String resourceUrl, HttpMethod httpMethod, ParameterizedTypeReference parameterizedTypeReference);

}
