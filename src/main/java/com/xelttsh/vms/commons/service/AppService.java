package com.xelttsh.vms.commons.service;

import java.util.List;
import java.util.Optional;

public interface AppService<T> {

    Optional<T> findById(Object id);

    List<T> findAll(int page, int size);

    List<T> findAll();

//    List<T> getAll();

    Optional<T> create(T t);

    Optional<T> update(T t);

    void delete(T t);

}
