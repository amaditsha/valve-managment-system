package com.xelttsh.vms.commons;

public enum MessageType {

	INFO, SUCCESS, WARNING, ERROR;

}
