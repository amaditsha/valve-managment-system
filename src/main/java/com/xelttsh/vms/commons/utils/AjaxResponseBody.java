package com.xelttsh.vms.commons.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AjaxResponseBody {

    String msg;
    String code;
    List<Object> result;
    Object body;

    //getters and setters

}
