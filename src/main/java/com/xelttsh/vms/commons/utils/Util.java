package com.xelttsh.vms.commons.utils;

import com.xelttsh.vms.commons.MessageType;
import com.xelttsh.vms.commons.enums.*;
import com.xelttsh.vms.commons.enums.security.AccessRight;
import lombok.extern.slf4j.Slf4j;
import java.security.SecureRandom;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Util {

    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String OTHER_CHAR = "!@#$%&*()_+-=[]?";

    private static final String PASSWORD_ALLOW_BASE = CHAR_LOWER + CHAR_UPPER + NUMBER + OTHER_CHAR;
    // optional, make it more random
    private static final String PASSWORD_ALLOW_BASE_SHUFFLE = shuffleString(PASSWORD_ALLOW_BASE);
    private static final String PASSWORD_ALLOW = PASSWORD_ALLOW_BASE_SHUFFLE;

    private static SecureRandom random = new SecureRandom();

    public static String generateRandomPassword(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {

            int rndCharAt = random.nextInt(PASSWORD_ALLOW.length());
            char rndChar = PASSWORD_ALLOW.charAt(rndCharAt);

            // debug
//            System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

            sb.append(rndChar);

        }

        return sb.toString();

    }

    // shuffle
    public static String shuffleString(String string) {
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);
        return letters.stream().collect(Collectors.joining());
    }

    public static boolean isRoleSelected(AccessRight accessRight, List<AccessRight> selectedRights) {
        return selectedRights.contains(accessRight);
    }


    public static String resolveAlertMessageTypeClasses(MessageType messageType) {
        switch (messageType) {
            case SUCCESS:
                return "alert-success";
            case ERROR:
                return "alert-danger";
            case WARNING:
                return "alert-warning";
            case INFO:
                return "alert-info";
            default:
                return "alert-primary";
        }
    }

    public static String resolvePaymentCreditValidityStatusClasses(PaymentCreditValidityStatus paymentCreditValidityStatus) {
        switch (paymentCreditValidityStatus) {
            case INVALID:
                return "badge-danger";
            default:
                return "badge-primary";
        }
    }

    public static String resolveEntityStatusClasses(EntityStatus entityStatus) {
        switch (entityStatus) {
            case ACTIVE:
                return "badge-success";
            case DISAPPROVED:
                return "badge-warning";
            case DELETED:
                return "badge-danger";
            default:
                return "badge-primary";
        }
    }

    public static String resolveEntityStatusClasses(String entityStatus) {
        switch (entityStatus) {
            case "ACTIVE":
                return "badge-success";
            case "DISAPPROVED":
                return "badge-warning";
            case "DELETED":
                return "badge-danger";
            default:
                return "badge-primary";
        }
    }

    public static String resolveUserStatusClasses(Status status) {
        if(status == null) {
            return "badge-primary";
        }
        switch (status) {
            case ACTIVE:
                return "badge-success";
            case INACTIVE:
                return "badge-warning";
            case DELETED:
                return "badge-danger";
            default:
                return "badge-primary";
        }
    }

    public static String isEmpty(String value) {
        if(value == null || value.isEmpty()) {
            return null;
        }
        return value;
    }

}
