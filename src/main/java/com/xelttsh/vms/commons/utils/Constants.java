package com.xelttsh.vms.commons.utils;

public class Constants {
    public static final String SESSION_USERNAME = "SESSION_USERNAME";
    public static final String SESSION_EMAIL = "SESSION_EMAIL";
    public static final String SESSION_PDF_PRODUCTS = "SESSION_PDF_PRODUCTS";
    public static final String DEFAULT_ADMIN = "admin";
}
