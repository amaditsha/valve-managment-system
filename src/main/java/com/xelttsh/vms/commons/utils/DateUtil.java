package com.xelttsh.vms.commons.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
@Slf4j
public class DateUtil {
    public static String formatDate(LocalDateTime date) {
        if(date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy, HH:mm");
        return formatter.format(date);
    }
    public static String formatDate(LocalDate date) {
        if(date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        return formatter.format(date);
    }

    public static String formatDisplayDate(LocalDateTime date){

        if(date == null) {
            log.info("::: DateUtil :: formatDisplayDate : Date is null/empty");
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
         String formatedDate = formatter.format(date);

        log.info("::: DateUtil :: formatDisplayDate : DATE: {}", formatedDate);
         return formatedDate;
    }

    public static String formatDisplayDate(LocalDate date){

        if(date == null) {
            log.info("::: DateUtil :: formatDisplayDate : Date is null/empty");
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
         String formatedDate = formatter.format(date);

        log.info("::: DateUtil :: formatDisplayDate : DATE: {}", formatedDate);
         return formatedDate;
    }
}
