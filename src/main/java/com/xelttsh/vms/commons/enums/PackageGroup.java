package com.xelttsh.vms.commons.enums;

public enum PackageGroup {
    LITE, BASIC, STANDARD, PREMIUM
}
