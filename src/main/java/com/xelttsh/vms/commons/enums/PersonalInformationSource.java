package com.xelttsh.vms.commons.enums;

public enum PersonalInformationSource {
    ECOCASH{
        @Override
        public boolean isEditable() {
            return false;
        }
    }, CUSTOMER{
        @Override
        public boolean isEditable() {
            return true;
        }
    };

    public abstract boolean isEditable();
}
