package com.xelttsh.vms.commons.enums;

public enum PaymentCreditValidityStatus {

	VALID("VALID"), INVALID("INVALID");
	private String value;

	private PaymentCreditValidityStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
