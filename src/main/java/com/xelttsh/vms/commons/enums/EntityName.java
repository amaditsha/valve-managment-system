package com.xelttsh.vms.commons.enums;

public enum EntityName {
    Agent, ServiceProvider
}
