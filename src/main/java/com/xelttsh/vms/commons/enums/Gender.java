package com.xelttsh.vms.commons.enums;

public enum Gender {
    MALE("male"), FEMALE("female"), UNDEFINED("undefined");

    private String value;

    private Gender(final String value) {
        this.value = value;
    }

    public static Gender fromValue(final String value) {
        for (Gender gender : Gender.values()) {
            if (gender.getValue().equalsIgnoreCase(value)) {
                return gender;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
