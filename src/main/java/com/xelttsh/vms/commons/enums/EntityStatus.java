package com.xelttsh.vms.commons.enums;

public enum EntityStatus {
    ACTIVE, DELETED, DISAPPROVED, DRAFT;
}
