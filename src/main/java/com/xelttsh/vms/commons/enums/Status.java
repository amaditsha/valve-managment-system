package com.xelttsh.vms.commons.enums;

public enum Status {
    ACTIVE, INACTIVE, DELETED, PENDING_ACTIVATION
}
