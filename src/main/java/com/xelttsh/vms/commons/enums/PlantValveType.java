package com.xelttsh.vms.commons.enums;

public enum PlantValveType {
    BOILER, TURBINE;
}
