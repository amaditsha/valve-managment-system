package com.xelttsh.vms.commons.enums;

public enum ProductPackageStatus {
    ACTIVE , DELETED , RETIRED;
}
