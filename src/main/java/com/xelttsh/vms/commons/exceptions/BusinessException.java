package com.xelttsh.vms.commons.exceptions;

import lombok.Data;

@Data
public class BusinessException extends  RuntimeException {
    private int code;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(int code, String message) {
        super(message);
        this.code = code;
    }
}
