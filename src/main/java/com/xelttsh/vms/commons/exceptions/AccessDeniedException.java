package com.xelttsh.vms.commons.exceptions;

import lombok.Data;

@Data
public class AccessDeniedException extends  RuntimeException {
    private int code;

    public AccessDeniedException(String message) {
        super(message);
    }

    public AccessDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessDeniedException(int code, String message) {
        super(message);
        this.code = code;
    }
}
