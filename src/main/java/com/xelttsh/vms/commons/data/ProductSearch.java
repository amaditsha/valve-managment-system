package com.xelttsh.vms.commons.data;

import com.xelttsh.vms.commons.enums.EntityStatus;
import com.xelttsh.vms.commons.enums.PlantValveType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductSearch {
    private String alphaNumeric;
    private LocalDateTime dateFrom;
    //    private String dateFromString;
    private LocalDateTime dateTo;
    private String valveType;
    private String kksNo;
    private String service;
    private EntityStatus status;
    private PlantValveType type;
}
