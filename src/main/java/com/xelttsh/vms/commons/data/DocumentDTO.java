package com.xelttsh.vms.commons.data;

import lombok.Data;

@Data
public class DocumentDTO {
    private byte[] file;
    private String fileName;
    private String description;
    private String entityId;
    private String entityName;
}
