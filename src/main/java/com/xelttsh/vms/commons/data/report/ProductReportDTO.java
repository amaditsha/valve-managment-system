package com.xelttsh.vms.commons.data.report;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductReportDTO {
    private String alphaNumeric;
    private String inspectionDate;
    private Integer boilerUnit;
    private String partDescription;
    private String stockNo;
    private Integer partQty;
    private String inspectionDecission;
    private String comments;
}
