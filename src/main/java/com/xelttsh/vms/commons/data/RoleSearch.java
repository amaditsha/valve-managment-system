package com.xelttsh.vms.commons.data;

import lombok.Data;

@Data
public class RoleSearch {
    private String name;
}
