package com.xelttsh.vms.commons.data;

import lombok.Data;

@Data
public class ApiResponse<T> {
    private int status;
    private String message;
    private T body;
}
