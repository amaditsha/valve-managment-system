package com.xelttsh.vms.commons.data;

import lombok.Data;

@Data
public class UserSearch {
    private String username;
}
