package com.xelttsh.vms.mail;

public interface EmailService {
    void sendEmail(String text, String subject, String email);
}
